#define MyAppName "SmartCodeCompletion"
#define MyAppVersion "alpha"

[Setup]
AppCopyright=Copyright � 2015 Honza Rames
AppId={{937D3C26-3C76-4953-BA48-85D381C584FA}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher=Honza Rames
DefaultDirName={pf}\{#MyAppName}
DefaultGroupName={#MyAppName}
OutputBaseFilename={#MyAppName}-{#MyAppVersion}Setup
Compression=lzma
SolidCompression=yes
VersionInfoVersion=0.0.1.0

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Components]
;Name: DelphiXE; Description: DelphiXE; Types: full; Check: IsVersionInstalled('8.0')
;Name: DelphiXE2; Description: DelphiXE2; Types: full; Check: IsVersionInstalled('9.0')
;Name: DelphiXE3; Description: DelphiXE3; Types: full; Check: IsVersionInstalled('10.0')
;Name: DelphiXE4; Description: DelphiXE4; Types: full; Check: IsVersionInstalled('11.0')
Name: DelphiXE5; Description: DelphiXE5; Types: full; Check: IsVersionInstalled('12.0')
Name: DelphiXE6; Description: DelphiXE6; Types: full; Check: IsVersionInstalled('14.0')
Name: DelphiXE7; Description: DelphiXE7; Types: full; Check: IsVersionInstalled('15.0')
Name: DelphiXE8; Description: DelphiXE8; Types: full; Check: IsVersionInstalled('16.0')
Name: DelphiXE9; Description: Seattle; Types: full; Check: IsVersionInstalled('17.0')

[Files]
;Source: "Release\XE\SmartCodeCompletion.dll"; DestName: "SmartCodeCompletionXE.dll"; DestDir: "{app}"; Components: DelphiXE; Flags: ignoreversion
;Source: "Release\XE2\SmartCodeCompletion.dll"; DestName: "SmartCodeCompletionXE2.dll"; DestDir: "{app}"; Components: DelphiXE2; Flags: ignoreversion
;Source: "Release\XE3\SmartCodeCompletion.dll"; DestName: "SmartCodeCompletionXE3.dll"; DestDir: "{app}"; Components: DelphiXE3; Flags: ignoreversion
;Source: "Release\XE4\SmartCodeCompletion.dll"; DestName: "SmartCodeCompletionXE4.dll"; DestDir: "{app}"; Components: DelphiXE4; Flags: ignoreversion
Source: "Release\XE5\SmartCodeCompletion.dll"; DestName: "SmartCodeCompletionXE5.dll"; DestDir: "{app}"; Components: DelphiXE5; Flags: ignoreversion
Source: "Release\XE6\SmartCodeCompletion.dll"; DestName: "SmartCodeCompletionXE6.dll"; DestDir: "{app}"; Components: DelphiXE6; Flags: ignoreversion
Source: "Release\XE7\SmartCodeCompletion.dll"; DestName: "SmartCodeCompletionXE7.dll"; DestDir: "{app}"; Components: DelphiXE7; Flags: ignoreversion
Source: "Release\XE8\SmartCodeCompletion.dll"; DestName: "SmartCodeCompletionXE8.dll"; DestDir: "{app}"; Components: DelphiXE8; Flags: ignoreversion
Source: "Release\Seattle\SmartCodeCompletion.dll"; DestName: "SmartCodeCompletionSeattle.dll"; DestDir: "{app}"; Components: DelphiXE9; Flags: ignoreversion

[Registry]

; Register the plugin dll
;Root: HKCU; Subkey: {code:ExpertsKey|8.0}; Components: DelphiXE; ValueType: string; ValueName: {#MyAppName}; ValueData: "{app}\SmartCodeCompletionXE.dll"; Flags: uninsdeletevalue
;Root: HKCU; Subkey: {code:ExpertsKey|9.0}; Components: DelphiXE2; ValueType: string; ValueName: {#MyAppName}; ValueData: "{app}\SmartCodeCompletionXE2.dll"; Flags: uninsdeletevalue
;Root: HKCU; Subkey: {code:ExpertsKey|10.0}; Components: DelphiXE3; ValueType: string; ValueName: {#MyAppName}; ValueData: "{app}\SmartCodeCompletionXE3.dll"; Flags: uninsdeletevalue
;Root: HKCU; Subkey: {code:ExpertsKey|11.0}; Components: DelphiXE4; ValueType: string; ValueName: {#MyAppName}; ValueData: "{app}\SmartCodeCompletionXE4.dll"; Flags: uninsdeletevalue
Root: HKCU; Subkey: {code:ExpertsKey|12.0}; Components: DelphiXE5; ValueType: string; ValueName: {#MyAppName}; ValueData: "{app}\SmartCodeCompletionXE5.dll"; Flags: uninsdeletevalue
Root: HKCU; Subkey: {code:ExpertsKey|14.0}; Components: DelphiXE6; ValueType: string; ValueName: {#MyAppName}; ValueData: "{app}\SmartCodeCompletionXE6.dll"; Flags: uninsdeletevalue
Root: HKCU; Subkey: {code:ExpertsKey|15.0}; Components: DelphiXE7; ValueType: string; ValueName: {#MyAppName}; ValueData: "{app}\SmartCodeCompletionXE7.dll"; Flags: uninsdeletevalue
Root: HKCU; Subkey: {code:ExpertsKey|16.0}; Components: DelphiXE8; ValueType: string; ValueName: {#MyAppName}; ValueData: "{app}\SmartCodeCompletionXE8.dll"; Flags: uninsdeletevalue
Root: HKCU; Subkey: {code:ExpertsKey|17.0}; Components: DelphiXE9; ValueType: string; ValueName: {#MyAppName}; ValueData: "{app}\SmartCodeCompletionSeattle.dll"; Flags: uninsdeletevalue


[Code]
const
  BDSRoot = 'Software\Embarcadero\BDS\';
  
function ExpertsKey(param: string): string;
begin
  Result := BDSRoot + param + '\Experts';
end;

function IsVersionInstalled(version: string): Boolean;
begin
  Result := RegKeyExists(HKCU, BDSRoot + version);
end;
