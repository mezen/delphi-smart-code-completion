# SmartCodeCompletion #

BDS expert that uses Levenshtein distance to compute code completion auto-suggestions, this allows to find partial or misspelled matches.

See [wiki](../../wiki) for more details.

Installing
----------

Clone the sources and run `Build.exe` it will find installed IDEs and allow you to build and register the plugin inside each IDE.

The pre-built installer is considered deprecated.

I took inspiration in Spring4D build mechanism https://bitbucket.org/sglienke/spring4d.

License
-------
This Source Code Form is subject to the terms of the Mozilla Public License,
v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain
one at http://mozilla.org/MPL/2.0/.